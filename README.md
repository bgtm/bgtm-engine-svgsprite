# BGTM - Engine for Generating an SVG sprite.

This is an engine which can be used in conjunction with
[BGTM](https://www.npmjs.com/package/bgtm) for
the purposes of processing a source directory of svgs to a svg sprite
file.

# How to use

Read the how to section for [BGTM](https://www.npmjs.com/package/bgtm).

Run:

    npm install --save-dev bgtm-engine-svgsprite

In your gulpfile add the following dependency:

    var svgsprite = require('bgtm-engine-svgsprite');

Then add the following task.

    tm.add('svg', {
        runOnBuild: true,
        watch: true,
        watchSource: [
            'src/svg/**/*'
        ],
        liveReload: true,
        engine: svgsprite,
        engineOptions: {
            'src': 'src/svg/**/*',
            'dest': 'dest/svg/',
            'spriteFileName': 'sprite.svg'
        }
    });

# What it does

* Gets the source files specified in the `src` property.
* Compiles these into a sprite using the package [gulp-svg-sprites](https://www.npmjs.com/package/gulp-svg-sprites)
* Runs the package [gulp-imagemin](https://www.npmjs.com/package/gulp-imagemin) on
  the sprite to ensure that the sprite is optimised.
* Outputs the result to the destination specified in the `dest` property
  with the filename specified in spriteFileName.

# Engine Options

## Defaults

    {
        src: '',
        dest: '',
        spriteFileName: ''
    }

| Option         | Mandatory | Description |
|----------------|-----------|-------------|
| src            | yes       | The source path to look for changes, This should be a string which can be pattern. |
| dest           | yes       | The destination file for the output to be placed into. |
| spriteFileName | no        | The name to output the file to in the dest path. |

# License

Copyright 2017 Ben Blanks

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.