var plumber = require('gulp-plumber');
var svgSprite = require('gulp-svg-sprite');
var deepExtend = require('deep-extend');
var imageMin = require('gulp-imagemin');

/**
 * This the default engine for building SASS into a consolidated build
 * for many projects.
 *
 * This ensures consistent build practices across many developers within a team.
 *
 * @param taskManager
 * @param args
 * @returns {*}
 */
module.exports = function (taskManager, args, done) {
    var defaults = {
        src: '',
        dest: '',
        spriteFileName: 'sprite.svg',
        mode: 'symbols'
    };
    var engineOptions = deepExtend({}, defaults, args);

    var svgConfig = {
        mode: {
            css: false, // Create a «css» sprite
            view: false, // Create a «view» sprite
            defs: false, // Create a «defs» sprite
            symbol: false, // Create a «symbol» sprite
            stack: false // Create a «stack» sprite
        }
    };

    if(engineOptions.mode === 'symbols') {
        svgConfig.mode.symbol = {
            dest: './',
            sprite: engineOptions.spriteFileName
        };
        return this.src(engineOptions.src)
            .pipe(plumber())
            .pipe(svgSprite(svgConfig))
            .pipe(imageMin(
                [
                    imageMin.svgo({
                        plugins: [
                            {cleanupIDs: false}
                        ]
                    })
                ],
                {
                    verbose: true
                }))
            .pipe(this.dest(engineOptions.dest))
            .pipe(done());
    }
    else if(engineOptions.mode === 'defs') {
        svgConfig.mode.defs = {
            dest: './',
            sprite: engineOptions.spriteFileName
        };
        return this.src(engineOptions.src)
            .pipe(plumber())
            .pipe(svgSprite(svgConfig))
            .pipe(imageMin(
                [
                    imageMin.svgo({
                        plugins: [
                            {removeUselessDefs: false}, // This plugin removes the defs from the SVG
                            {cleanupIDs: false}
                        ]
                    })
                ],
                {
                    verbose: true
                }))
            .pipe(this.dest(engineOptions.dest))
            .pipe(done());
    }
};